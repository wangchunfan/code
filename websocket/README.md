# 概述

应用场景：
- 实现消息推送，替代 ajax 轮询。
- 实现聊天功能

WebSocket 是 HTML5 提供的在单个 TCP 链接上进行全双工通讯的协议。

客户端请求
- Connection: upgrade 表示客户端希望升级连接
- Upgrade: websocket 表示希望升级到 websocket 协议

```http request
Request URL: ws://127.0.0.1:8080/webSocket/88274.2421418903
Request Method: GET
Connection: Upgrade
Upgrade: websocket
Sec-WebSocket-Version: 13
Sec-WebSocket-Key: Uzqa3dqq8zeBv2Ol4nxOaQ==
Sec-WebSocket-Extensions: permessage-deflate; client_max_window_bits
```

服务端响应

```http request
Upgrade: websocket
Connection: Upgrade
Sec-WebSocket-Accept: Fl7B8k4erUi8bempzvN91WtKtqk=
Sec-WebSocket-Extensions: permessage-deflate;client_max_window_bits=15
```



# HTML5 中的 WebSocket
WebSocket 的属性

- ws.readyState 当前连接状态
    - 0 连接未建立
    - 1 连接已建立，可以正常通信
    - 2 连接正在关闭
    - 3 连接已关闭或连接不能打开
- ws.bufferedAmount 已被 send() 放入正在队列中等待传输，但是还没有发出的 UTF-8 文本字节数

WebSocket 的事件
- ws.onopen 建立连接后的回调函数
- ws.onmessage 收到服务端消息的回调函数
- ws.onerror 通信错误的回调函数
- ws.onclose 连接关闭的回调函数

WebSocket 的方法
- ws.send(data) 发送数据
- ws.close() 关闭连接


使用

```javascript
// 创建链接
var ws = new WebSocket(url, [protocol] );
// 建立连接后的回调函数
ws.onopen = function (event) {
};
// 收到服务端消息的回调函数
ws.onmessage = function (event) {
};
// 通信错误的回调函数
ws.onerror = function (event) {
};
// 连接关闭的回调函数
ws.onclose = function (event) {
}

// 发送数据
function send() {
    ws.send(JSON.stringify(message));
}

// 关闭连接
function closeWebSocket() {
    ws.close();
}
```

### 问题 1

>Failed to construct 'WebSocket': The URL's scheme must be either 'ws' or 'wss'. 'localhost' is not allowed.

url 协议不要忽略 `ws` 或 `wss` 分别对应 `http` 与 `https` 的区别

# 在 springboot 中访问静态页面

index.html 文件放置位置 resource/static/index.html

直接访问地址：localhost:8080/index.html

springboot 在 resource 文件夹下有两个默认文件夹
- static 存放静态页面
- templates 存放动态页面，需要配合 thymeleaf 使用

# Java 支持 WebSocket

添加 spring-boot-starter-websocket 依赖
```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-websocket</artifactId>
</dependency>
```
定义监听端点,和监听事件(与前端的监听事件相同)
```java
@ServerEndpoint("/webSocket/{sid}")
@Component
public class WebSocketServer {
    // 事件
     @OnOpen
     public void onOpen(Session session, @PathParam("sid") String sid) {}
     @OnMessage
     public void onMessage(Session session, String message) {}
     @OnError
     public void onError(Session session, Throwable error) {}
     @OnClose
     public void onClose() {}
    // 方法
    private Session session;    // 客户端连接对象
    public void fun(){
        // 发送消息
        this.session.getBasicRemote().sendText(message);  //同步发送
        this.session.getAsyncRemote().sendText(message); //异步发送
        // 关闭连接
        this.session.close();
    }
}
```

### 问题 1

>index.html:22 WebSocket connection to 'ws://127.0.0.1:8080/webSocket/10000' failed: Error during WebSocket handshake: Unexpected response code: 404

在使用 springboot 内置的 Tomcat 容器时，需要注入 ServerEndpointExporter，这个 bean 中的 registerEndpoints() 方法会将 @ServerEndpoint 注解的 bean 添加到 serverContainer 容器中。

ServerEndpointExporter 是由 spring-boot-starter-websocket 提供的，并且这个项目的子项目中包含 starter-web，所以单独做一个 websocket 时只需要依赖 starter-websocket 即可

```java
@Configuration
public class WebSocketConfig {
    /**
     * 使用内置 tomcat 需要定义 bean
     * 外置则不需要，否则会报错
     */
    @Bean
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }
}
```

如果还是失败，需要检查是否将这个类注册为 bean。需要添加 @Component 注解，@ServerEndpoint 不会生成一个 bean,它是 Tomcat 中的注解，表示服务监听端点的地址。

```java
@ServerEndpoint("/webSocket/{sid}")
@Component
public class WebSocketServer {}
```

因为 ServerEndpointExporter 注册时是从所有 **bean** 中查找 

```java
protected void registerEndpoints() {
    String[] endpointBeanNames = context.getBeanNamesForAnnotation(ServerEndpoint.class);
}
```
### 问题 2

> 连接错误 导致 连接断开

在服务端的 onMessage 方法中抛出未捕获异常会触发 onError 事件，然后触发 onClose 事件。最终也会触发客户端的 onClose 事件。

客户端 onMessage 方法中异常不会触发其它监听事件。

### 问题 3

时间久了什么都不做，服务端会触发 onError 时间，导致连接断开

```
java.io.EOFException
 	at org.apache.tomcat.util.net.NioEndpoint$NioSocketWrapper.fillReadBuffer(NioEndpoint.java:1289)
 	at org.apache.tomcat.util.net.NioEndpoint$NioSocketWrapper.read(NioEndpoint.java:1223)
 	at org.apache.tomcat.websocket.server.WsFrameServer.onDataAvailable(WsFrameServer.java:72)
 	at org.apache.tomcat.websocket.server.WsFrameServer.doOnDataAvailable(WsFrameServer.java:171)
 	at org.apache.tomcat.websocket.server.WsFrameServer.notifyDataAvailable(WsFrameServer.java:151)
 	at org.apache.tomcat.websocket.server.WsHttpUpgradeHandler.upgradeDispatch(WsHttpUpgradeHandler.java:148)
 	at org.apache.coyote.http11.upgrade.UpgradeProcessorInternal.dispatch(UpgradeProcessorInternal.java:54)
 	at org.apache.coyote.AbstractProcessorLight.process(AbstractProcessorLight.java:53)
 	at org.apache.coyote.AbstractProtocol$ConnectionHandler.process(AbstractProtocol.java:806)
 	at org.apache.tomcat.util.net.NioEndpoint$SocketProcessor.doRun(NioEndpoint.java:1498)
 	at org.apache.tomcat.util.net.SocketProcessorBase.run(SocketProcessorBase.java:49)
 	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1149)
 	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
 	at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)
 	at java.lang.Thread.run(Thread.java:748)
```

解决方案：维持心跳检测
- 客户端隔一段时间向服务端发送心跳连接，防止超时未发送过消息导致断开
- 如果连接断开就重新创建链接

### 问题 4

在重启服务端时出现

错误1：
```
java.lang.IllegalStateException: The remote endpoint was in state [TEXT_FULL_WRITING] which is an invalid state for called method
```
原因：当客户端连接后，会群发当前在线人数的消息给其它客户端。
如果当前有客户端 A 在线，然后客户端 B、C 同时连接获取，并且同时使用客户端 A 的 session 向 A 发送消息，
同一时刻，多个线程向一个 socket 写数据冲突引发以上异常。解决方案，在使用 session 发送消息时加锁。

```java
synchronized (session) {
    this.session.getBasicRemote().sendText(message);
}
```

错误2：
```
java.lang.NullPointerException: Deflater has been closed
```

错误3：
```
java.lang.IllegalStateException: Message will not be sent because the WebSocket session has been closed
```
原因：使用了 CopyAndWriteArraySet 维护 session 对象，读写可能会产生短暂的不一致。
在群发消息时可能获取到了一个关闭的 session。
解决方案：让读取、发送 与 删除、新增 操作互斥？这样不能解决问题，因为连接断开是无法加锁的。
这样的异常可以捕获然后忽略掉。


---

参考：
- [HTML5 WebSocket](https://www.runoob.com/html/html5-websocket.html)
- [SpringBoot如何返回页面](https://www.cnblogs.com/guo-xu/p/11203740.html)
- [SpringBoot跳转页面详解+thymeleaf](https://blog.csdn.net/jintingbo/article/details/81633615)
- [用JAVA分别实现WebSocket客户端与服务端](https://www.cnblogs.com/jieerma666/p/10342435.html)
