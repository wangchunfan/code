package wang.chunfan.code.websocket.controller;

import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import wang.chunfan.code.websocket.model.Message;
import wang.chunfan.code.websocket.server.WebSocketServer;

@RestController
@RequestMapping
public class WebController {
    @Autowired
    WebSocketServer webSocketServer;

    @GetMapping
    public String index() {
        return "你好";
    }

    /**
     * 群发消息
     */
    @GetMapping("/sendGroup/{data}")
    public void sendGroup(@PathVariable String data) {
        Message message = new Message();
        message.setData(data);
        message.setType("group");
        webSocketServer.sendMessage(JSON.toJSONString(message), null);
    }
}
