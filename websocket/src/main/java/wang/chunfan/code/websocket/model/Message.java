package wang.chunfan.code.websocket.model;

public class Message {
    // 消息类型
    private String type;
    // 消息内容
    private String data;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
