package main

// example 1
// type Interface1 interface {
// 	M1(string)
// }
// type Interface2 interface {
// 	Interface1
// 	M1(string)
// }

// example 2
// func main() {
// 	type T struct{}
// 	var t T

// 	var i interface{} = 15 //ok
// 	i = "hello golang"     //ok
// 	i = t                  //ok
// 	i = &t                 //ok
// 	println(i)
// }

// example 3
// type QuackableAnimal interface {
// 	Quack()
// }

// type Duck struct{}

// type Dog struct{}

// func (Duck) Quack() {
// 	println("duck quack")
// }

// func (Dog) Quack() {
// 	println("dog quack")
// }

// func AnimalQuackInForest(a QuackableAnimal) {
// 	a.Quack()
// }

// func main() {
// 	animals := []QuackableAnimal{new(Duck), new(Dog)}
// 	for _, animal := range animals {
// 		AnimalQuackInForest(animal)
// 	}
// }

// // duck quack
// // dog quack
