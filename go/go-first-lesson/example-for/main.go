package main

// func main() {
// 	// example 1
// 	// var sum int
// 	// for i := 0; i < 10; i++ {
// 	// 	sum += i
// 	// }
// 	// println(sum) // 45

// 	// example 2
// 	// for i := 0; i < 10; {
// 	// 	i++
// 	// }

// 	// i := 0
// 	// for ; i < 10; i++ {

// 	// }

// 	// for i < 10 {

// 	// }

// 	// for {

// 	// }

// 	// example 3
// 	// sl := []int{1, 2, 3, 4}
// 	// for i, v := range sl {
// 	// 	print(i, v)
// 	// }

// 	// for i := range sl {
// 	// 	println(i)
// 	// }

// 	// for _, v := range sl {
// 	// 	println(v)
// 	// }

// 	// for _, _ = range sl {

// 	// }

// 	// for range sl {

// 	// }

// 	// example 4
// 	// arr := [3]int{1, 2, 3}
// 	// for i, v := range arr {
// 	// 	println(i, v)
// 	// }
// }

// example 5
// func main() {
// 	var sum int
// 	var sl = []int{1, 2, 3, 4, 5}

// 	for i := 0; i < len(sl); i++ {
// 		if sl[i]%2 == 1 {
// 			continue
// 		}
// 		sum += sl[i]
// 	}
// 	println(sum) //6
// }

// example 6
// func main() {
// 	var sum int
// 	var sl = []int{1, 2, 3, 4, 5}
// loop:
// 	for i := 0; i < len(sl); i++ {
// 		if sl[i]%2 == 1 {
// 			continue loop
// 		}
// 		sum += sl[i]
// 	}
// 	println(sum) //6
// }

// example 7
// func main() {
// 	var sl = [][]int{
// 		{1, 43, 56, 23},
// 		{2, 43, 12, 33},
// 		{12, 43, 3, 45},
// 		{34, 54, 9, 12},
// 	}
// loop:
// 	for i := 0; i < len(sl); i++ {
// 		for j := 0; j < len(sl[i]); j++ {
// 			if sl[i][j] == 12 {
// 				print(i, j, ";") //12;20;33;
// 				continue loop
// 			}
// 		}
// 	}
// }

// example 8
// func main() {
// 	sl := []int{1, 2, 3, 4}
// 	for i, v := range sl {
// 		if v == 3 {
// 			println(i)
// 			break
// 		}
// 	}
// }

// example 9
func main() {
	sl := [][]int{
		{1, 2, 3, 4},
		{5, 6, 7, 8},
		{9, 10, 11, 12},
	}
loop:
	for i := 0; i < len(sl); i++ {
		for j := 0; j < len(sl[i]); j++ {
			if sl[i][j] == 6 {
				println(i, j) //1 1
				break loop
			}
		}
	}
}
