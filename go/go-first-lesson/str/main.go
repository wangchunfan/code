package main

import "fmt"

func main() {
	// var s string = "北京\n你好"
	// println(s)

	// 	var ss string = `北京\n
	// 你好`
	// 	println(ss)
	// fmt.Printf("the length of s = %d\n", len(s))

	// for i := 0; i < len(s); i++ {
	// 	fmt.Printf("0x%x ", s[i])
	// }

	// fmt.Println("the character count in s is", utf8.RuneCountInString(s))
	// for cc, c := range s {
	// 	fmt.Printf("0x%x ", c)
	// 	fmt.Print(" ", cc)
	// }

	// var c rune = '中'
	// fmt.Printf("0x%x", c)

	// var s = "\u5317 \u4eac"
	// println(s)

	// a := 'a'
	// b := '中'
	// c := '\n'
	// d := '\''

	// a := '\u5317'     //字符：中 16位表示
	// b := '\U00005317' //字符：中 32 位表示
	// c := '\u0027'     //单引号字符
	// var c rune = '北'
	// fmt.Printf("0x%x", c) // 0x5317

	a := "北京"            // 使用字符表示 北京
	b := "\u5317京"       // 使用 Uncode 编码表示 北
	c := "\xe5\x8c\x97京" // 使用 UTF-8 编码表示 北
	// d := "\b111001011000110010010111"

	println(a)
	println(b)
	println(c)
	fmt.Printf("%b ", 0xe5)
	fmt.Printf("%b ", 0x8c)
	fmt.Printf("%b ", 0x97)
	// println(d)
}
