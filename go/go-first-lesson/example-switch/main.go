package main

// example 1
// func main() {
// 	i := 2
// 	switch a, b, c := 1, 2, 3; i {
// 	case a:
// 		println(a)
// 	case b:
// 		println(b)
// 	default:
// 		println(c)
// 	}
// }

// example 2

// func main() {
// 	sl := []int{1, 2, 3, 4, 5}
// 	for i, v := range sl {
// 		switch v % 2 {
// 		case 0:
// 			println(i, v)
// 			break
// 		case 1:

// 		}
// 	}
// }

// example 3
// func main() {
// 	var x interface{} = 23
// 	switch x.(type) {
// 	case nil:
// 		println("x is nil")
// 	case int:
// 		println("the type of x is int")
// 	case string:
// 		println("the type of x is string")
// 	}
// 	// the type of x is int
// }

// example 4
func main() {
	var x interface{} = 23
	switch v := x.(type) {
	case nil:
		println("x is nil")
	case int:
		println("the type of x is int", v)
	case string:
		println("the type of x is string", v)
	}
	// the type of x is int 23
}
