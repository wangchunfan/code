package main

// example 1
// var myPrint = func(str string) {
// 	println(str)
// }

// func main() {
// 	myPrint("hello")
// }

// example 2
// func dofunc(f func()) {
// 	f()
// }

// func main() {
// 	dofunc(func() { println("hello") })
// }

// example 3
// func getPrint(str string) func() {
// 	return func() {
// 		println(str)
// 	}
// }

// func main() {
// 	f := getPrint("hello")
// 	f()
// }

var f func() = func() {}
