package main

import (
	"fmt"

	_ "github.com/wangchunfan/prog-init-order/pkg1"
	_ "github.com/wangchunfan/prog-init-order/pkg3"
)

var (
	_ = constInitCheck()
	_ = variableInit("v")
)

const (
	c = "c"
)

func constInitCheck() string {
	if c != "" {
		fmt.Println("main: const c has been initialized")
	}
	return ""
}

func variableInit(name string) string {
	fmt.Printf("main: var %s has been initialized\n", name)
	return name
}

func init() {
	fmt.Println("main: init1 func invoked")
}

func init() {
	fmt.Println("main: init2 func invoked")
}

func main() {
	fmt.Println("main: exe main")
}
