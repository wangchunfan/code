package main

import (
	"unsafe"

	"example.type/book"
)

type (
	T2 T1
	T1 int
)

type Event struct{}

type T3 struct { //error: illegal cycle in declaration of T3
	T3
}

type T4 struct { //error: illegal cycle in declaration of T4
	T5
}

type T5 struct { //error: illegal cycle in declaration of T4
	T4
}

type T6 struct {
	pt *T6
	at []T6
	mt map[string]T6
}

func main() {
	var a T1 = 1
	var b T2 = 2

	println(a)
	println(b)

	var book = book.Book{}
	println(book.Author.Addr) //访问作者1的住址
	println(book.Addr)        //访问作者2的住址

	var e Event
	println(unsafe.Sizeof(e)) //0 无内存占用
	var c = make(chan Event)
	c <- Event{}

}
