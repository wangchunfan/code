package book

type Persion struct {
	Name  string
	Phone string
	Addr  string
}

type Book struct {
	Title   string         //书名
	Pages   int            //页数
	Indexes map[string]int //索引
	Author  Persion        //作者1 使用其它结构体
	Persion                //作者2 嵌入字段
	_       int            // 使用空标识符作为字段名称
}
