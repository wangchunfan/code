package main

// example 1
// const Pi float64 = 3.1415926

// const (
// 	size    int = 12
// 	a, b, c     = 2, 4.0, "str"
// )

// func main() {
// 	println(Pi, size, a, b, c)
// }

// example 2
// type myInt int

// const n myInt = 12
// const m int = n + 5

// func main() {
// 	a := 5
// 	print(a + n)
// }

// example 3
// type myInt int

// const n = 12
// const m int = n + 5

// func main() {
// 	a := 5
// 	print(a + n)
// }

// example 4

// const a = 123456789

// func main() {
// 	var b int8 = 1
// 	print(b + a)
// }

// example 5

// const (
// 	a, b = 1, 2
// 	c, d
// )

// func main() {
// 	println(a, b, c, d)	//1 2 1 2
// }

//example 6
const (
	a    = iota
	b, c = iota, iota + 1
	d, e
)

func main() {
	println(a, b, c, d, e) //0 1 2 2 3
}
