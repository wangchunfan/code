package main

import (
	"fmt"
	"unsafe"
)

func main() {
	// example 1
	// var nums = []int{1, 2, 3, 4, 5, 6}
	// println(len(nums))
	// println(cap(nums))

	//example 2
	// sl := make([]int, 4, 10)
	// println(len(sl))
	// println(cap(sl))

	//example 3
	// sl := make([]int, 5)
	// println(len(sl)) //5
	// println(cap(sl)) //5

	//example 4
	// arr := [...]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	// sl := arr[3:7:9]

	// println(len(sl), cap(sl)) //4 6
	// fmt.Println(sl)           //[4 5 6 7]

	// sl[1] = 0
	// sl = append(sl, 22)
	// fmt.Println(sl)  //[4 0 6 7]
	// fmt.Println(arr) //[1 2 3 4 0 6 7 8 9 10]

	//example 5
	// sl1 := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	// sl2 := sl1[0:8:8]
	// fmt.Println(sl1) // [1 2 3 4 5 6 7 8 9 10]
	// fmt.Println(sl2) // [1 2 3 4 5 6 7 8]

	// sl2[0] = 0
	// fmt.Println(sl1) // [0 2 3 4 5 6 7 8 9 10]
	// fmt.Println(sl2) //[0 2 3 4 5 6 7 8]

	// example 6
	// sl := []int{1, 2, 3, 4}
	// fmt.Println(len(sl), cap(sl)) // 4 4
	// sl = append(sl, 5)
	// fmt.Println(len(sl), cap(sl)) // 5 8

	// example 7
	// arr := []int{1, 2, 3, 4}
	// sl := arr[0:4:4]
	// fmt.Println(sl) //[1 2 3 4]

	// sl[0] = 0
	// sl = append(sl, 5)
	// sl[1] = 0
	// fmt.Println(arr) // [0 2 3 4]
	// fmt.Println(sl)  //[0 0 3 4 5]

	// example 8
	// var nums []int
	// fmt.Println(nums, nums == nil)
	// nums = append(nums, 43)
	// fmt.Println(nums)

	// example 9
	// var nums = []int{1, 2, 3, 4, 5, 6}
	// for i := 0; i < len(nums); i++ {
	// 	fmt.Print(nums[i], ";")
	// }
	// println()
	// for k, v := range nums {
	// 	fmt.Print(k, v, ";")
	// }

	// example 10
	var s1 = []int{1, 2, 3}
	var s2 = []int{1, 2, 3, 4, 5}
	print(s1)
	print(s2)
}

type slice struct {
	array unsafe.Pointer // 指向底层数组的指针
	len   int            // 切片长度，切片中元素个数
	cap   int            //切片容量，也就是底层数组的实际长度
}

func print(s []int) {
	fmt.Println(s)
}
