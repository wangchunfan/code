package main

import (
	_ "github.com/go-redis/redis/v7" // 空导入
	"github.com/google/uuid"
	logrus "github.com/sirupsen/logrus"
)

func main() {
	logrus.Info("info msg")
	logrus.Println(uuid.NewString())
}
