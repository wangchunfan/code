module github.com/wangcf/moduleversion

go 1.19

require (
	github.com/go-redis/redis/v7 v7.4.1
	github.com/google/uuid v1.3.0
	github.com/sirupsen/logrus v1.8.0
)

require (
	github.com/magefile/mage v1.10.0 // indirect
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
)
