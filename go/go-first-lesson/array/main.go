package main

import "fmt"

func main() {
	// example 1
	// var arr = [2]int64{1, 2}
	// println("数组长度", len(arr))                 // 数组长度 2
	// println("数组大小", unsafe.Sizeof(arr), "字节") // 数组大小 16 字节

	// example 2
	// var arr1 [2]int64                    //[0, 0]
	// var arr2 [2]int64 = [2]int64{2, 3}   //[2, 3]
	// var arr3 = [2]int64{1, 2}            //[1, 2]
	// var arr4 = [2]int64{1}               //[1, 0]
	// var arr5 = [...]int{1, 2, 3, 4, 5}   //[1, 2, 3, 4, 5]
	// var arr6 = [...]int{4: 3, 3: 4}      // [0 0 0 4 3]
	// var arr7 = [2][2]int{{1, 2}, {1, 2}} //[[1 2] [1 2]]

	// fmt.Println(arr1)
	// fmt.Println(arr2)
	// fmt.Println(arr3)
	// fmt.Println(arr4)
	// fmt.Println(arr5)
	// fmt.Println(arr6)
	// fmt.Println(arr7)

	//example 3
	var arr1 = [4]int{1, 2, 3, 4}
	var arr2 = [5]int{1, 2, 3, 4, 5}
	var arr3 = [4]int{1, 2, 3, 4}
	var arr4 = [4]int{1, 2, 3, 5}
	foo(arr2)
	println(arr1 == arr3)
	println(arr1 == arr4) //false
}

func foo(arr [5]int) {
	fmt.Println(arr)
}
