package main

func main() {
	//example 1
	// a, b := false, true
	// if a && b != true {
	// 	println("a && b first")
	// } else {
	// 	println("b != true first")
	// }

	// example 2
	// if a, c := 1, 3; a < 0 {
	// 	// println(a, b, c) error: undefined: b
	// 	println(a, c)
	// } else if b := 2; b < 0 {
	// 	println(b, c)
	// } else {
	// 	println(a, b, c) //1 2 3
	// }
	// // println(a, b, c) //error: undefined: a undefined: b  undefined: c

	// example 3
	if false {
		return
	}
	if false {
		return
	}
	// do some thing true
}
