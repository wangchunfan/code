package main

// example 1
// import (
// 	"fmt"
// 	"time"
// )

// func main() {
// 	go fmt.Println("I am a goroutine")

// 	go func(a, b int) {
// 		println(a + b)
// 	}(3, 4)

// 	time.Sleep(time.Second * 60)
// }

// // 7
// // I am a goroutine

// example 2
// func main() {
// 	// c := make(chan int)
// 	var c chan int
// 	c = make(chan int)
// 	go func(a, b int) {
// 		time.Sleep(2 * time.Second)
// 		c <- a + b
// 	}(3, 4)

// 	println(<-c)
// 	println("end ...")
// }

// example 3
// func main() {
// 	ch := make(chan int)
// 	ch <- 1
// 	n := <-ch
// 	print(n)
// }
// fatal error: all goroutines are asleep - deadlock!

// func main() {
// 	ch := make(chan int)
// 	go func() { ch <- 1 }()
// 	n := <-ch
// 	print(n)
// }

// example 4
// func main() {
// 	ch := make(chan int, 1)
// 	ch <- 1
// 	n := <-ch
// 	print(n) //1
// }

// example 5
// func main() {
// 	ch := make(chan int, 1)
// 	ch <- 1
// 	ch <- 2
// 	n := <-ch
// 	println(n)
// }

// example 6
// func main() {
// 	ch1 := make(chan<- int, 1) // 只发送
// 	ch2 := make(<-chan int, 1) // 只接受

// 	<-ch1    // invalid operation: cannot receive from send-only channel ch1 (variable of type chan<- int)
// 	ch2 <- 1 // invalid operation: cannot send to receive-only channel ch2 (variable of type <-chan int)
// }

// func Out(ch <-chan int) {
// 	n := <-ch
// 	println(n)
// }

// func In(ch chan<- int) {
// 	ch <- 1
// 	close(ch)
// }
// func main() {
// 	ch := make(chan int, 1)
// 	In(ch)
// 	Out(ch)
// }

func main() {
	select {
	case x := <-ch1: // 从 ch1 中接收数据
	case y, ok := <-ch2: //从 ch2 中接收数据
	case ch3 <- n: //将 n 变量的值发送到 ch3中
	default: //以上通信都被阻塞时执行
	}
}
