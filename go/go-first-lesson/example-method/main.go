package main

// example 1
// type Book struct {
// 	Title string
// }

// func (book Book) getTitle() string {
// 	return book.Title
// }

// func main() {
// 	book1 := Book{"Go"}
// 	println(book1.getTitle())

// 	book2 := &Book{"Java"}
// 	println(book2.getTitle())
// }

// example 2
// type myint = int

// func (i myint) get() myint {
// 	return i
// }

// type mystr string

// func (s mystr) get() mystr {
// 	return s
// }

// func main() {
// 	// myint := 2
// 	// println(myint)
// 	mystr := "helo"
// 	println(mystr.get())
// }
