package wang.chunfan.code.netty.nio;

/**
 * @version 1.0
 * @Author wangchunfan
 * @Date 2020/6/1 11:07
 **/
public class TimeServer {
    public static void main(String[] args) {
        int port = 8080;
        MultiplexerTimeServer multiplexerTimeServer = new MultiplexerTimeServer(port);
        // 在一个单独线程中运行多路复用器 Selector
        new Thread(multiplexerTimeServer, "NIO-MultiplexerTimeServer-001").start();
    }
}
