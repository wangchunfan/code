package wang.chunfan.code.netty.nio;

/**
 * @version 1.0
 * @Author wangchunfan
 * @Date 2020/6/1 11:55
 **/
public class TimeClient {
    public static void main(String[] args) {
        int port = 8080;
        new Thread(new TimeClientHandle("127.0.0.1", port), "TimeClient-001").start();
    }
}
