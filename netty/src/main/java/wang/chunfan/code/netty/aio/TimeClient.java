package wang.chunfan.code.netty.aio;

/**
 * @version 1.0
 * @Author wangchunfan
 * @Date 2020/6/2 15:56
 **/
public class TimeClient {
    public static void main(String[] args) {
        int port = 8080;
        new Thread(new AsyncTimeClientHandler("127.0.0.1", port),
                "AIO-AsyncTimeClientHandler-001").start();
    }
}
