package wang.chunfan.code.netty.aio;

/**
 * @version 1.0
 * @Author wangchunfan
 * @Date 2020/6/2 13:53
 **/
public class TimeServer {
    public static void main(String[] args) {
        int port = 8080;
        AsyncTimeServerHandler timeServer = new AsyncTimeServerHandler(port);
        new Thread(timeServer, "AIO-AsyncTimeServerHandler-001").start();

    }
}
