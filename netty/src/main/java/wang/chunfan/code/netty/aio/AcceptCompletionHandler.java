package wang.chunfan.code.netty.aio;

import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;

/**
 * @version 1.0
 * @Author wangchunfan
 * @Date 2020/6/2 14:00
 * 处理连接请求
 **/
public class AcceptCompletionHandler implements CompletionHandler<AsynchronousSocketChannel, AsyncTimeServerHandler> {


    @Override
    public void completed(AsynchronousSocketChannel socketChannel, AsyncTimeServerHandler attachment) {
        // 接收到客户端连接后，需要再次接收新的连接
        // 不能再外部的 while 循环中写，因为是异步的，应该在回调函数中处理
        attachment.asynchronousServerSocketChannel.accept(attachment, this);
        // 将 channel 中的信息读取到 buffer 中
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        // 异步读取，增加回调
        // 第一个 buffer 为目标 buffer
        // 第二个参数是附加对象 attachment
        // 第三个参数是回调函数
        socketChannel.read(buffer, buffer, new ReadCompletionHandler(socketChannel));
    }

    @Override
    public void failed(Throwable exc, AsyncTimeServerHandler attachment) {
        exc.printStackTrace();
        attachment.latch.countDown();
    }
}
