package wang.chunfan.code.netty.async;

import wang.chunfan.code.netty.bio.TimeServerHandler;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @version 1.0
 * @Author wangchunfan
 * @Date 2020/6/1 9:32
 * 伪异步 I/O 服务端
 **/
public class TimeServer {
    public static void main(String[] args) {
        try (ServerSocket server = new ServerSocket(8080)) {
            System.out.println("The time server is start in port 8080");
            Socket socket = null;
            TimeServerHandlerExecutePool singleExecutor = new TimeServerHandlerExecutePool(50, 10000);
            while (true) {
                socket = server.accept();
                // 使用 bio 中的 TimeServerHandler，返回一个实现 Runnable 接口的对象
                // 放到线程池中执行
                singleExecutor.execute(new TimeServerHandler(socket));
                // 在 bio 中，直接创建线程执行
                // new Thread(new TimeServerHandler(socket)).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
