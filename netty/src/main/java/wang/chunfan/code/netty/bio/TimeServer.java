package wang.chunfan.code.netty.bio;

import javax.jws.soap.SOAPBinding;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @version 1.0
 * @Author wangchunfan
 * @Date 2020/5/29 17:01
 **/
public class TimeServer {
    public static void main(String[] args) throws IOException {
        int port = 8080;
        try (ServerSocket server = new ServerSocket(port)) {
            System.out.println("The time server is start in port:" + port);
            Socket socket = null;
            while (true) {
                socket = server.accept();
                new Thread(new TimeServerHandler(socket)).start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
