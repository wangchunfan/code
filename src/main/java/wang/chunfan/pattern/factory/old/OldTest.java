package wang.chunfan.pattern.factory.old;

import wang.chunfan.pattern.factory.Food;
import wang.chunfan.pattern.factory.ShouZhuaBing;

public class OldTest {
    public static void main(String[] args) {
        Food shouZhuaBing = new ShouZhuaBing();
        shouZhuaBing.eaten();
    }
}