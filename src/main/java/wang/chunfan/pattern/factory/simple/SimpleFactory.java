package wang.chunfan.pattern.factory.simple;

import wang.chunfan.pattern.factory.Food;
import wang.chunfan.pattern.factory.ShouZhuaBing;

public class SimpleFactory {
    public static Food createFood(String foodName) {
        Food food = null;
        if ("手抓饼".equals(foodName)) {
            food = new ShouZhuaBing();
        } else if ("其它食物。。。".equals(foodName)) {
            // food = new XXX
        }
        return food;
    }
}
