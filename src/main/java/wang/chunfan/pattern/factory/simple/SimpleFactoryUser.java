package wang.chunfan.pattern.factory.simple;

import wang.chunfan.pattern.factory.Food;

public class SimpleFactoryUser {
    public static void main(String[] args) {
        Food shouZhuaBing = SimpleFactory.createFood("手抓饼");
        shouZhuaBing.eaten();
    }
}
