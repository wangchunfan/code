package wang.chunfan.pattern.factory;

public interface Food {
    void eaten();
}
