package wang.chunfan.pattern.factory.abstractfactory;

import wang.chunfan.pattern.factory.Food;

public abstract class AbstractFactory {

    abstract Food createShouZhuangbing();

    abstract Food createKaoLengMian();
}
