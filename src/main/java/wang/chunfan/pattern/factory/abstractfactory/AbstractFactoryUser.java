package wang.chunfan.pattern.factory.abstractfactory;

import wang.chunfan.pattern.factory.Food;

public class AbstractFactoryUser {
    public static void main(String[] args) {
        AbstractFactory factory = new Factory();
        Food kaoLengMian = factory.createKaoLengMian();
        Food shouZhuangbing = factory.createShouZhuangbing();

        kaoLengMian.eaten();
        shouZhuangbing.eaten();
    }
}
