package wang.chunfan.pattern.factory.abstractfactory;

import wang.chunfan.pattern.factory.Food;
import wang.chunfan.pattern.factory.KaoLengMian;
import wang.chunfan.pattern.factory.ShouZhuaBing;

public class Factory extends AbstractFactory {
    @Override
    Food createShouZhuangbing() {
        return new ShouZhuaBing();
    }

    @Override
    Food createKaoLengMian() {
        return new KaoLengMian();
    }
}
