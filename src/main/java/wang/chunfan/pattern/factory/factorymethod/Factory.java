package wang.chunfan.pattern.factory.factorymethod;

import wang.chunfan.pattern.factory.Food;

public interface Factory {
    Food createFood();
}
