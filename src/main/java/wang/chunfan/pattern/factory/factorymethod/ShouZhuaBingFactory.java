package wang.chunfan.pattern.factory.factorymethod;

import wang.chunfan.pattern.factory.Food;
import wang.chunfan.pattern.factory.ShouZhuaBing;

public class ShouZhuaBingFactory implements Factory{
    public Food createFood() {
        return new ShouZhuaBing();
    }
}
