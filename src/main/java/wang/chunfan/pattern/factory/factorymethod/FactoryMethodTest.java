package wang.chunfan.pattern.factory.factorymethod;

import wang.chunfan.pattern.factory.Food;

public class FactoryMethodTest {
    public static void main(String[] args) {
        KaoLengMianFactory kaoLengMianFactory = new KaoLengMianFactory();
        Food kaoLengMian = kaoLengMianFactory.createFood();
        kaoLengMian.eaten();

        ShouZhuaBingFactory shouZhuaBingFactory = new ShouZhuaBingFactory();
        Food shouZhuaBing = shouZhuaBingFactory.createFood();
        shouZhuaBing.eaten();
    }
}
