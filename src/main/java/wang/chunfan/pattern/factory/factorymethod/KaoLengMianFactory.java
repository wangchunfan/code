package wang.chunfan.pattern.factory.factorymethod;

import wang.chunfan.pattern.factory.Food;
import wang.chunfan.pattern.factory.KaoLengMian;

public class KaoLengMianFactory implements Factory{
    public Food createFood() {
        return new KaoLengMian();
    }
}
