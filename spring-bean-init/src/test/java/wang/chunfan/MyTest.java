package wang.chunfan;


import org.junit.Test;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MyTest {
    @Test
    public void myTest() {
        AbstractApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        applicationContext.getBean(User.class);
        // 只有关闭容器，才会调用 destroy 方法
        applicationContext.registerShutdownHook();
    }
}
