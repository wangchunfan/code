package wang.chunfan.mybatis.pojo;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorLog extends AbstractLog {
    public ErrorLog() {
        setType("errorLog");
    }

    private String errorInfo;
}
