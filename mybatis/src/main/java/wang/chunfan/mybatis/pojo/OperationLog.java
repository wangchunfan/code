package wang.chunfan.mybatis.pojo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OperationLog extends AbstractLog {
    public OperationLog(){
        setType("operationLog");
    }

    /**
     * 请求地址
     */
    private String requestUri;

    /**
     * 请求方法
     */
    private String requestMethod;

    /**
     * 请求参数
     */
    private String requestParams;

    /**
     * 请求时长（毫秒）
     */
    private Long requestTime;


}
