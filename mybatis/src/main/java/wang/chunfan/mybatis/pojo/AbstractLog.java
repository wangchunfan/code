package wang.chunfan.mybatis.pojo;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public abstract class AbstractLog {
    private String type;

    /**
     * 操作用户
     */
    Long userId;

    /**
     * 操作
     */
    private String operation;

    /**
     * 操作状态 成功，失败
     */
    private String status;

    /**
     * 用户代理
     */
    private String userAgent;

    private String ip;

    /**
     * 操作时间
     */
    private Date createTime;


}
