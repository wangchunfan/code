package wang.chunfan.mybatis.pojo;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Login {
    Long userId;
    String password;
}
