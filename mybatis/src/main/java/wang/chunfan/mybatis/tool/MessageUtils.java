package wang.chunfan.mybatis.tool;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContext;
import org.springframework.context.i18n.LocaleContextHolder;

public class MessageUtils {
    private static MessageSource messageSource;

    static {
        messageSource = (MessageSource) SpringContextUtils.getBean("messageSource");
    }

    public static String getMessage(int code) {
        return getMessage(code, new String[0]);
    }

    public static String getMessage(int code, String... params) {
        // LocaleContextHolder.getLocale() 从 request header 中获取
        // Accept-Language: zh-CN,zh;q=0.9
        return messageSource.getMessage(String.valueOf(code), params, LocaleContextHolder.getLocale());
    }

}
