package wang.chunfan.mybatis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import wang.chunfan.mybatis.annotation.LogOperation;
import wang.chunfan.mybatis.pojo.User;
import wang.chunfan.mybatis.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @PutMapping
    @LogOperation("新增用户")
    public int insert(@RequestBody User user) {
        return userService.insert(user);
    }

    @GetMapping("/{id}")
    public User get(@PathVariable Long id) {
        return userService.get(id);
    }
}
