package wang.chunfan.mybatis.redis;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import javafx.scene.shape.VLineTo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.*;
import org.springframework.stereotype.Component;
import wang.chunfan.mybatis.pojo.User;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

@Component
public class UserRedis {

    @Autowired
    RedisTemplate redisTemplate;

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    public void setUser(User user) {
        redisTemplate.opsForValue().set(getUserKey(user.getId()), user);
    }

    public User getUser(Long id) {
        User user = (User) redisTemplate.opsForValue().get(getUserKey(id));
        return user;
    }

    private static String getUserKey(Long id) {
        return "user:" + id;
    }

}
