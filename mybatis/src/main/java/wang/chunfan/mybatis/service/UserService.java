package wang.chunfan.mybatis.service;

import wang.chunfan.mybatis.pojo.User;

public interface UserService {
    int insert(User user);

    User get(Long id);

    int delete(Long id);

    int update(User user);
}
