package wang.chunfan.mybatis.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import wang.chunfan.mybatis.dao.UserDao;
import wang.chunfan.mybatis.exception.MyException;
import wang.chunfan.mybatis.pojo.User;
import wang.chunfan.mybatis.redis.UserRedis;
import wang.chunfan.mybatis.service.UserService;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;
    @Autowired
    UserRedis userRedis;

    private static final Object lock = new Object();

    @Override
    public int insert(User user) {
        return userDao.insert(user);
    }

    @Override
    public User get(Long id) {
        User user = userRedis.getUser(id);
        if (user == null) {
            user = userDao.get(id);
            if (user != null) {
                userRedis.setUser(user);
            }
        }

        if (user == null) {
            throw new MyException(10002);
        }

        return user;
    }

    @Override
    public int delete(Long id) {
        return 0;
    }

    @Override
    public int update(User user) {
        return 0;
    }


}
