package wang.chunfan.mybatis.dao;

import wang.chunfan.mybatis.pojo.ErrorLog;

public interface ErrorLogDao extends BaseDao<ErrorLog>{
    @Override
    int insert(ErrorLog errorLog);
}
