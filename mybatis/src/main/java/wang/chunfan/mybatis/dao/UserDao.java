package wang.chunfan.mybatis.dao;

import wang.chunfan.mybatis.pojo.User;

public interface UserDao extends BaseDao<User> {
    @Override
    int insert(User user);

    User get(Long id);

    int update(User user);

    int delete(Long id);

}
