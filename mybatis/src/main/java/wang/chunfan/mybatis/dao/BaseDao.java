package wang.chunfan.mybatis.dao;

public interface BaseDao<T> {
    int insert(T entity);
}
