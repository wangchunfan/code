package wang.chunfan.mybatis.dao;

import wang.chunfan.mybatis.pojo.LoginLog;
import wang.chunfan.mybatis.pojo.OperationLog;

public interface OperationLogDao extends BaseDao<OperationLog> {
    @Override
    int insert(OperationLog operationLog);
}
