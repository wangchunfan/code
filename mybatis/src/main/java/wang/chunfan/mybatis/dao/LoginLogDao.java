package wang.chunfan.mybatis.dao;

import wang.chunfan.mybatis.pojo.LoginLog;

public interface LoginLogDao extends BaseDao<LoginLog> {
    @Override
    int insert(LoginLog loginLog);
}
