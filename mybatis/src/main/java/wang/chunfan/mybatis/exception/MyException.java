package wang.chunfan.mybatis.exception;

import lombok.Getter;
import lombok.Setter;
import wang.chunfan.mybatis.tool.MessageUtils;

@Getter
@Setter
public class MyException extends RuntimeException {
    private int code;
    private String msg;

    public MyException(int code) {
        this.code = code;
        this.msg = MessageUtils.getMessage(code);
    }

    public MyException(String msg) {
        this.code = 500;
        this.msg = msg;
    }

    public MyException(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}
